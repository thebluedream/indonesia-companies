from mongoengine import connect
from mongoengine import Document, StringField, DynamicDocument
from mongoengine.fields import IntField

connection = connect(db="IndonesiaCompany", host="localhost", port=27017)
if connection:
    print("Succesfully Connect to database")

class companyData(Document):
    stockCode = StringField(unique= True)
    companyName = StringField()
    companySector  = StringField()
    subSector = StringField()
    stockPrice = IntField()
    marketCap  = StringField()

class companyDataCRUD():
    def showAllCompany(self):
        try:
            result = []
            for doc in companyData.objects:
                company = {
                    "stockCode" : doc.stockCode,
                    "companyName" : doc.companyName,
                    "companySector" : doc.companySector,
                    "subSector" : doc.subSector,
                    "stockPrice" : doc.stockPrice,
                    "marketCap" : doc.marketCap
                }
                result.append(company)
            return result
        except Exception as e:
            print(e)
    def insertCompany(self, **param):
        try:
            companyData(**param).save()
        except Exception as e:
            print(e)
    def deleteCompany(self, param):
        try:
            doc = companyData.objects(stockCode = param).first()
            doc.delete()
        except Exception as e:
            print(e)
    def updateCompany(self, param):
        try:
            doc = companyData.objects(stockCode = param['stockCode']).first()
            doc.companyName = param['companyName']
            doc.companySector = param['companySector']
            doc.subSector = param['subSector']
            doc.stockPrice = param['stockPrice']
            doc.marketCap = param['marketCap']
            doc.save()
        except Exception as e:
            print(e)
    def companyByStockCode(self, param):
        try:
            items = companyData.objects(stockCode = param).first()
            company = {
                "stockCode" : items.stockCode,
                "companyName" : items.companyName,
                "companySector" : items.companySector,
                "subSector" : items.subSector,
                "stockPrice" : items.stockPrice,
                "marketCap" : items.marketCap
            }
            return company
        except Exception as e:
            print(e)
            
    def companyBySector(self, param):
        try:
            result = []
            for doc in companyData.objects(companySector = param):
                company = {
                    "stockCode" : doc.stockCode,
                    "companyName" : doc.companyName,
                    "companySector" : doc.companySector,
                    "subSector" : doc.subSector,
                    "stockPrice" : doc.stockPrice,
                    "marketCap" : doc.marketCap
                }
                result.append(company)  
            return result
        except Exception as e:
            print(e)


        '''
                    result = []
            for doc in companyData.objects:
                user = {
                    "stockCode" : doc.stockCode,
                    "companyName" : doc.companyName,
                    "companySector" : doc.companySector,
                    "subSector" : doc.subSector,
                    "stockPrice" : doc.stockPrice,
                    "marketCap" : doc.marketCap
                }
                result.append(user)
            return result
            
        '''
