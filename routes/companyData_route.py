from fastapi import APIRouter
from api.companyData_api import *

router = APIRouter()

@router.get('/showall')
async def showAllCompany():
    return show_allCompany()

@router.post('/insert')
async def insertNewCompany(param:dict):
    result = insert_Company(**param)
    return result

@router.post('/delete')
async def deleteCompanyByStockCode(param:dict):
    result = delete_byStockCode(param['stockCode'])
    return result

@router.post('/update')
async def updateCompanyByStockCode(param:dict):
    result = update_byStockCode(param)
    return result

@router.post('/showbystock')
async def showCompanyByStockCode(param:dict):
    return show_companyByStockCode(param['stockCode'])

@router.post('/showbysector')
async def showCompanyBySector(param:dict):
    return show_companyBySectorName(param['companySector'])