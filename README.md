**MONGODB APP**

**SIMPLE BACKEND DATABASE INDONESIA STOCKS USING MONGODB**

Ini adalah aplikasi sederhana yang menampilkan database saham perusahaan terbuka di di Indonesia. Aplikasi ini dibuat dengan menggunakan MongoDb, FastAPI, dan ODM Mongoengine. 

**Penggunaan**
1. Pastikan kalian sudah menginstal package yang ada di **requirement.txt** agar aplikasi dapat dijalankan dengan semestinya
2. Data di **companyData.json** adalah sampel data yang dapat digunakan
3. Mengaktifkan server dengan `uvicorn main:app --reload` di terminal
3. Perintah yang dapat digunakan terdapat di bawah 

<details><summary>Menampilkan semua data</summary>

Menampilkan seluruh data perusahaan dengan cara mengakses GET pada postman ***localhost:8000/showall***

</details>

<details><summary>Menambahkan data perusahaan</summary>

Menambahkan data perusahaan dengan cara mengakses POST pada postman ***localhost:8000/insert***. 
Contoh :  
`{
    "stockCode": "BBCA",
    "companyName": "Bank Central Asia Tbk.",
    "companySector": "Financial",
    "subSector": "Banks",
    "stockPrice": 30800,
    "marketCap": "759.37 T"
}`

</details>

<details><summary>Menghapus data perusahaan dengan kode saham</summary>

Menghapus data perusahaan dengan cara mengakses POST pada postman ***localhost:8000/delete***. 
Contoh :  
`{
    "stockCode": "BBCA"
}`

</details>

<details><summary>Memperbarui data perusahaan</summary>

Memperbarui data perusahaan dengan cara mengakses POST pada postman ***localhost:8000/update***. 
Contoh :  
`{
    "stockCode": "BBCA",
    "companyName": "Bank Central Asia Tbk.",
    "companySector": "Financial",
    "subSector": "Banks",
    "stockPrice": 6500,
    "marketCap": "800 T"
}`

</details>

<details><summary>Mencari data perusahaan berdasarkan kode saham</summary>

Mencari data perusahaan berdasarkan kode saham dengan cara mengakses POST pada postman ***localhost:8000/showbystock***. 
Contoh :  
`{
    "stockCode": "EMTK",
}`

</details>

<details><summary>Mencari data perusahaan di suatu sektor berdasarkan kode saham</summary>

Mencari data perusahaan di suatu sektor berdasarkan kode saham dengan cara mengakses POST pada postman ***localhost:8000/showbysector***. 
Contoh :  
`{
    "companySector": "Energy"
}`

</details>


Project tugas akhir sanbercode 

