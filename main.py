from fastapi import FastAPI
from routes.companyData_route import router as company_router
# from produk_route import router as produk_router

app = FastAPI()

app.include_router(company_router)
# app.include_router(produk_router)

@app.get("/")
async def read_main():
    return {"message": "Welcome"}