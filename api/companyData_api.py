from pymongo import MongoClient
from models.companyData_model import companyDataCRUD as db
import csv, json
import json
from bson import ObjectId

db = db()

def ObjIdToStr(obj):
    return str(obj['_id'])

def insert_Company(**param):
    db.insertCompany(**param)
    data = {
        "message": "Company succesfully added!"
    }
    return data

def show_allCompany():
    result = db.showAllCompany()
    return result

def delete_byStockCode(param):
    db.deleteCompany(param)
    data = {
        "message": "Company succesfully deleted!"
    }
    return data

def update_byStockCode(param):
    db.updateCompany(param)
    data = {
        "message": "Company succesfully updated!"
    }
    return data

def show_companyByStockCode(param):
    result = db.companyByStockCode(param)
    return result

def show_companyBySectorName(param):
    result = db.companyBySector(param)
    return result